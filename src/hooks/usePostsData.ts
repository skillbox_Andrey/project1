import { useState, useEffect } from "react"
declare const window: any
export function usePostsData() {
  const [listPosts, setlistPosts] = useState({})
  useEffect(() => {
    if (window.__listPosts__) {
      setlistPosts(window.__listPosts__)
    }
  }, [])

  return [listPosts]
}