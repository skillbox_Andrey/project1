import { useContext, useEffect, useRef, useState } from "react";

export function useFormComment(bool: boolean) {
  const [IsOpen, setIsOpen] = useState(false)
  useEffect(() => {
    setIsOpen(bool)
  }, [])
  return [IsOpen]
}