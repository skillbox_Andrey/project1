import { Action, ActionCreator, AnyAction } from 'redux';
import { ThunkAction } from 'redux-thunk/es/types';

import { rootReducer, RootState } from '../store';

const UPDATE_TOKEN = 'UPDATE_TOKEN'

export const updateToken: ActionCreator<AnyAction> = (text) => ({
  type: UPDATE_TOKEN,
  text,
})


export const saveToken = (): ThunkAction<void, RootState, unknown, Action> => (dispatch) => {
  const token = window.__token__
  if (token.length > 5) {
    console.log('token', token, '12312')
    dispatch(updateToken(token))
  }
} 