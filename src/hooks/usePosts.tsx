import React, { useEffect, useState, useContext, createElement } from 'react'
import axios from 'axios'
import { TOKEN } from './useToken';
import { usePostsData } from './usePostsData';
// import { tokenContext } from '../shared/context/tokenContext';
import { Window } from '../../types/global';
import { MenuIcon } from '../shared/icons';
import { Dropdown } from '../shared/Dropdown';
import { MenuItemsList } from '../shared/CardList/Card/Menu/MenuItemsList';
import { DropList } from '../shared/DropList';
import { generateId } from '../utils/react/generateRandomIndex';
import { BlockIcon, WarningIcon, RepostIcon, SaveIcon, CommentIcon } from '../shared/icons';
import { Icon } from "../shared/icons";
import { EIcons } from "../shared/icons";
import styles from './posts.css';
import { useStore } from 'react-redux';
import { RootState } from '../store';

const LIST = [
  { value: 'Комментарии', src: 'src/images/1.svg' },
  { value: 'Поделиться', src: '../../../DropList/images/2' },
  { value: 'Скрыть', src: '../../../DropList/images/3' },
  { value: 'Сохранить', src: '../../../DropList/images/4' },
  { value: 'Пожаловаться', src: '../../../DropList/images/5' }
].map(generateId)

export var amountCards = 0

export function usePosts() {
  const [data, setData] = useState()
  const [isVisible, setIsVisible] = React.useState(false)
  const [title, setTitle] = React.useState('')

  useEffect(() => {
    axios.get(
      'https://oauth.reddit.com/best.json?sr_detail=true',
      {
        headers: {
          'Authorization': `bearer ${TOKEN}`
        }
      }
    ).then((resp) => {
      // console.log('324234', resp.data['children'])
      console.log(resp.data)
      const listPosts = resp.data
      setData(listPosts)
    })
      .catch(console.log)
  }, [TOKEN])
  // console.log('4324324', TOKEN)
  const DATA = data
  if (DATA != undefined) {
    // console.log('DATA', DATA['data']['children']['0']['data']['sr_detail'])
  }
  let listPostsElement
  function createElementsList() {

    if (DATA != undefined) {
      // console.log(DATA['data']['dist'])
      let localList = []
      for (let index = 0; index < DATA['data']['dist']; index++) {
        let objectElement = {}
        let item = DATA['data']['children'][`${index}`]
        // let srcAvatar = item['data']['thumbnail']
        // let userName = item['data']['author_fullname']
        // let postLink = item['data']['title']
        // let srcPreview = item['data']['sr_detail']['icon_img']
        objectElement = {
          'srcAvatar': item['data']['thumbnail'],
          'userName': item['data']['author_fullname'],
          'postLink': item['data']['title'],
          'srcPreview': item['data']['sr_detail']['icon_img']
        }
        localList.push(objectElement)
      }
      amountCards = localList.length
      return localList
    }
  }
  if (DATA != undefined) {
    listPostsElement = createElementsList()
    // console.log('listPostsElement', listPostsElement)
  }

  return [listPostsElement]
}