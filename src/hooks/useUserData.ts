import { useEffect } from "react";
import axios from "axios";
import { useDispatch, useSelector, useStore } from 'react-redux';
import { RootState } from "../store";
import { IUserData, meRequest, meRequestError, meRequestSuccess } from "../me/action";
import { meRequestAsync } from "../me/action";

export function useUserData() {
  const data = useSelector<RootState, IUserData>(state => state.me.data)
  const loading = useSelector<RootState, boolean>(state => state.me.loading)
  console.log(loading, 'fsdgfafgd')
  const store = useStore<RootState>()
  const token = store.getState().tokenText
  const dispatch = useDispatch()

  useEffect(() => {
    if (!token) return
    // dispatch(meRequest())
    dispatch(meRequestAsync() as any)
    // axios.get('https://oauth.reddit.com/api/v1/me',
    //   {
    //     headers: {
    //       Authorization: `bearer ${token}`
    //     }
    //   }).then((resp) => {
    //     const userData = resp.data
    //     dispatch(meRequestSuccess({ name: userData.name, iconImg: userData.icon_img }))
    //   }).catch((error) => {
    //     console.log(String(error))
    //     dispatch(meRequestError())
    //   })
  }, [token])

  return [{
    data,
    loading
  }]
}