import { atom } from "recoil";
import { create } from "zustand";

interface CommentStore {
  commentValue: string
  changeCommentValue: (text: string) => void
}

export const useCommentStore = create<CommentStore>((set) => ({
  commentValue: '',

  changeCommentValue: (text: string) => set(state => (
    {
      commentValue: text
    }
  ))
}))

interface NameStore {
  name: string
  setName: (text: string) => void
}

export const useNameStore = create<NameStore>((set) => ({
  name: '',

  setName: (text: string) => set(state => (
    {
      name: text
    }
  ))
}))
