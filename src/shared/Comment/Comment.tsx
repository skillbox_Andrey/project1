import React from 'react';
import axios from "axios";
import { useContext, useEffect, useState } from "react";
import styles from './comment.css';
import { CommentCard } from './CommentCard';
// import { tokenContext } from "../context/tokenContext";
import { CommentsCounter } from '../../hooks/CommentsCounter';
import { bigCommentsArr } from '../../hooks/bigCommentsArr';
// import { CommentForm } from '../CommentForm';
import { useStore } from 'react-redux';
import { RootState } from '../../store';


export function Comment() {
  // const token = useContext(tokenContext)
  const store = useStore<RootState>()
  const token = store.getState().tokenText
  let arr: any = new Set()
  const [commentArr, setCommentArr] = useState(arr)
  useEffect(() => {
    console.log(2, token, token !== undefined && token !== '' && token !== 'undefined')
    if (token !== undefined && token !== '' && token !== 'undefined') {
      console.log(1)
      if (CommentsCounter() == 1) {
        axios.get(`https://oauth.reddit.com/comments/113or6c`,
          {
            headers: {
              Authorization: `bearer ${token}`
            }
          }).then((resp) => {
            const commentData = resp.data
            for (let index = 0; index < 4; index++) {
              let ArrBig = []
              let arr = 0
              interface IdataMy {
                text: string,
                fullname: string,
                clan: string
                reply: any
              }
              let localArr = commentData[1]['data']['children'][index]['data']
              for (let i = 0; i < 3; i++) {
                console.log('bigCommentsArr', bigCommentsArr)
                console.log(localArr)
                console.log(localArr['replies'])
                if (localArr['replies'] != undefined) {
                  if (localArr['replies']['data'] != undefined && localArr['replies']['data'].hasOwnProperty('children') && localArr['replies']['data']['children'].length > 1) {
                    // console.log('1', localArr)
                    // console.log('2', localArr['replies']['data']['children'])
                    // console.log('3', localArr)
                    let dataMy: IdataMy = {
                      text: localArr['body'],
                      fullname: localArr['author_fullname'],
                      clan: localArr['name'],
                      reply: [],
                      // replies: localArr['replies']
                    }
                    // console.log('IdataMy', dataMy)
                    ArrBig.push(dataMy)
                    if (localArr['replies']['data'].hasOwnProperty('children') && localArr['replies']['data']['children'][0].hasOwnProperty('data')
                      && localArr['replies']['data']['children'].length > 1) {
                      console.log(localArr['replies'])
                      let local2Arr = localArr['replies']['data']['children'][1]['data']
                      localArr = local2Arr
                    }
                  }
                }
              }
              for (let i2 = 0; i2 < ArrBig.length - 1; i2++) {
                ArrBig[i2]['reply'] = [ArrBig[i2 + 1]]
              }

              console.log('ArrBig', ArrBig)
              // }
              // console.log('replies', commentData[1]['data']['children'][index]['data']['replies'])
              console.log(commentArr)
              setCommentArr([commentArr, ArrBig[0]])
            }
          }).catch(console.log)
      }
    }
    // console.log('3122222222222222222222214254333333333333333333333333333333333333323421244444444444444')
  }, [commentArr])
  // console.log('CommentArr', commentArr)
  // sosi.push(commentArr)
  // console.log('sosi', sosi)
  bigCommentsArr.push(commentArr)
  console.log(bigCommentsArr[0].length, 'bigCommentsArr[0].length')
  let commentCounter = 0
  let localbigCommentsArr = []
  for (let index = 0; index < bigCommentsArr.length; index++) {
    if (bigCommentsArr[index].length != undefined) {
      commentCounter++
      localbigCommentsArr.push(bigCommentsArr[index])
    }

  }
  console.log(commentCounter, 'commentCounter')
  console.log(localbigCommentsArr, 'localbigCommentsArr')
  return (
    <div className={styles.Comment}>
      {commentCounter >= 4 &&
        <CommentCard Comment={localbigCommentsArr[localbigCommentsArr.length - 4][1]} number={0} />
      }
      {commentCounter >= 4 &&
        <CommentCard Comment={localbigCommentsArr[localbigCommentsArr.length - 3][1]} number={0} />
      }
      {bigCommentsArr.length >= 4 &&
        <CommentCard Comment={localbigCommentsArr[localbigCommentsArr.length - 2][1]} number={0} />
      }
      {commentCounter >= 4 &&
        <CommentCard Comment={localbigCommentsArr[localbigCommentsArr.length - 1][1]} number={0} />
      }
    </div>
  );
}
