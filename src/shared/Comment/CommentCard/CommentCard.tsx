import styles from './commentcard.css';
import { ChangeEvent, FormEvent, useContext, useEffect, useRef, useState } from "react";
import { RepostIcon } from "../../icons";
import { CommentIcon } from "../../icons";
import { WarningIcon } from "../../icons";
// import { CommentForm } from "../../CommentForm";
import { bigCommentsArr } from '../../../hooks/bigCommentsArr';
import { RootState } from '../../../store';
// import { useDispatch, useSelector, useStore } from 'react-redux';
import { updateComment } from '../../../store';
import { Formik, Form, Field, ErrorMessage, useFormik } from 'formik';
// import * as Yup from 'yup'
import { useCommentStore, useNameStore } from '../../zustandStore';
// import { useRecoilState } from 'recoil'

interface ICommentCard {
  Comment: any
  number: number
}

export function CommentCard({ Comment, number }: ICommentCard) {
  // let valueStore = useSelector<RootState, string>(state => state.
  // const dispatch = useDispatch()

  const commentValue = useCommentStore(state => state.commentValue)
  const setCommentValue = useCommentStore(state => state.changeCommentValue)
  const name = useNameStore(state => state.name)
  const [isOpen, setIsOpen] = useState(false)
  let ref = useRef<HTMLDivElement>(null)
  let textInput = useRef<HTMLTextAreaElement>(null)
  let switchForm = false

  useEffect(() => {
    function handleClick(event: MouseEvent) {
      if (event.target instanceof Node && ref.current != null) {
        if (!ref.current?.contains(event.target) && switchForm) {
          setIsOpen(false)
          switchForm = false
        } else switchForm = true
      }
    }

    document.addEventListener('click', handleClick)

    return () => {
      document.removeEventListener('click', handleClick)
    }
  }, [])

  useEffect(() => {
    if (textInput.current != null) {
      textInput.current.focus()
    }
  })

  function addComment() {
    number++
    let fullname = name
    let repliceComment = {
      text: commentValue.slice(`${Comment['fullname']}, `.length - 1),
      fullname: fullname,
      clan: 'no',
      reply: 0
    }
    Comment['reply'].unshift(repliceComment)
  }

  const [isDisable, setIsDisable] = useState(true)
  const [isClick, SetIsClick] = useState(false)

  function validateMy() {
    // dispatch(updateComment(formik.values.comment))
    setCommentValue(formik.values.comment)
    // console.log(commentValue, 'zustand')
    console.log(1)
    if (formik.values.comment.slice(`${Comment['fullname']}, `.length - 1).length <= 3) {
      console.log(1234234234)
      let errors = {
        comment: ''
      }
      errors.comment = 'Введите более 3 символов'
      console.log(isClick, 'isClick')
      setIsDisable(false)
      return errors
    } else {
      setIsDisable(true)
      return
    }
  }

  function clickBtn() {
    console.log('12312')
    SetIsClick(true)
  }

  const formik = useFormik({
    initialValues: {
      comment: `${Comment['fullname']}, `,
    },
    onSubmit: values => {
      // valueStore = values.comment
      setCommentValue(values.comment)
      addComment()
      setIsOpen(false)
      validateMy()
      console.log('форма отправлена', values.comment)
    },

    validate: validateMy
  })


  return (
    <div className={styles.commentсard} id={String(Math.random)}>
      <span className={styles.leftLine}></span>
      <div className={styles.container}>
        <div className={styles.information}>
          <img src="" alt="" className={styles.imgAuthor} />
          <p className={styles.name}>{Comment['fullname']}</p>
          <p className={styles.time}>1 час назад</p>
          <p className={styles.clan}>{Comment['clan']}</p>
        </div>
        <p className={styles.text}>
          {Comment['text']}
        </p>
        <div className={styles.options}>
          <button className={styles.button} type="button" onClick={() => setIsOpen(true)}>
            <CommentIcon />
            <p className={styles.buttonText}>Ответить</p>
          </button>
          <button className={styles.button}>
            <RepostIcon />
            <p className={styles.buttonText}>Поделиться</p>
          </button>
          <button className={styles.button}>
            <WarningIcon />
            <p className={styles.buttonText}>Пожаловаться</p>
          </button>
        </div>
        <div>
          <div>
            {isOpen &&
              <div ref={ref}>
                <form className={styles.form} onSubmit={formik.handleSubmit} >
                  <textarea name='comment' ref={textInput} className={styles.inputForm} value={formik.values.comment} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                  <span className='error'>
                    {formik.errors.comment && formik.touched.comment && formik.errors.comment}
                  </span>
                  <button disabled={!isDisable && isClick} onClick={clickBtn} type='submit' className={styles.buttonForm}>Комментировать</button>
                </form>
              </div>
            }
          </div>
        </div>
      </div>
      {
        number < Comment['reply'].length &&
        Comment['reply'].map((item: any, index: number) => (
          < CommentCard Comment={Comment['reply'][index]} number={0} />
        )
        )
      }
    </div >
  )
}