import React from 'react';

interface IItem {
  id : string,
  value: string,
  src: string,
}

interface IMyListProps {
  list: IItem[]
}

export function DropList({ list }: IMyListProps) {
  const [isMounted] = useIsMounted()

  React.useEffect(() => {
    console.log('isMounted', isMounted)
  }, [isMounted])

  return (
    <ul>
      {
        list.map((item: IItem, index) =>
          <li key={item.id}>
            <img src={item.src}/>
            {item.value}
            </li>)
      }
    </ul>
  )
}

function useIsMounted() {
  const [isMounted, setIsMounted] = React.useState(false)

  React.useEffect(() => {
    setIsMounted(true)
  }, [])

  return [isMounted]
}