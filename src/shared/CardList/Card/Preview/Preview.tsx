import React from 'react';
import styles from './preview.css';

interface IPreviewProps {
  srcPreview: string
}

export function Preview({ srcPreview }: IPreviewProps) {
  return (
    <div className={styles.preview}>
      <img className={styles.previewImg} src={srcPreview} alt="" />
    </div>
  );
}
