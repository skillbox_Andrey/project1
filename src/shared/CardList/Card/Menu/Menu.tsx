import React, { useEffect, useRef, useState } from 'react';
import ReactDOM from "react-dom";
import { DropList } from '../../../DropList';
import { generateId } from "../../../../utils/react/generateRandomIndex";
import styles from './menu.css';
import { MenuIcon } from '../../../icons';
import { Dropdown } from '../../../Dropdown';
import { MenuItemsList } from "./MenuItemsList";

const LIST = [
  { value: 'Комментарии', src: 'src/images/1.svg' },
  { value: 'Поделиться', src: '../../../DropList/images/2' },
  { value: 'Скрыть', src: '../../../DropList/images/3' },
  { value: 'Сохранить', src: '../../../DropList/images/4' },
  { value: 'Пожаловаться', src: '../../../DropList/images/5' }
].map(generateId)

interface IMenuProps {
  MyId: string
}

export function Menu({ MyId }: IMenuProps) {
  const [isDropdownOpened, setIsDropdownOpened] = useState(false)
  const node = document.querySelector('#dropdown')

  if (!node) return null

  return (
    <div className={styles.menu}>
      <div className={styles.container}>
        <div id={MyId} onClick={() =>
          setIsDropdownOpened(true)
        }>
          <button className={styles.menuButton}>
            <svg width="5" height="20" viewBox="0 0 5 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <circle cx="2.5" cy="2.5" r="2.5" fill="#D9D9D9" />
              <circle cx="2.5" cy="10" r="2.5" fill="#D9D9D9" />
              <circle cx="2.5" cy="17.5" r="2.5" fill="#D9D9D9" />
            </svg>
          </button>
        </div>
        <div style={{ position: 'relative' }}>
          {isDropdownOpened && (
            <Dropdown postId='123' MyId={MyId} onClose={() => { setIsDropdownOpened(false) }} />
          )}
        </div>
      </div>
    </div >
  );
}
