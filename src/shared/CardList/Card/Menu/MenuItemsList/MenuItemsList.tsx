import React from 'react';
import { BlockIcon, WarningIcon, RepostIcon, SaveIcon, CommentIcon } from "../../../../icons";
import { Icon } from "../../../../icons";
import { EIcons } from "../../../../icons";
import styles from './menuitemslist.css';

interface IMenuItemsListProps {
  postId: string
}

export function MenuItemsList({ postId }: IMenuItemsListProps) {
  return (
    <ul className={styles.menuItemsList}>
      <li className={styles.menuItemDisplay} onClick={() => console.log(postId)}>
        {/* <CommentIcon /> */}
        <Icon name={EIcons.Comment} sizeHeight={14} sizeWidth={14} />
        <p className={styles.text}>Комментарии</p>
      </li>

      <div className={styles.divider} />

      <li className={styles.menuItemDisplay} onClick={() => console.log(postId)}>
        <Icon name={EIcons.Repost} sizeHeight={14} sizeWidth={12} />
        <p className={styles.text}>Поделиться</p>
      </li>

      <div className={styles.divider} />

      <li className={styles.menuItem} onClick={() => console.log(postId)}>
        <Icon name={EIcons.Block} sizeHeight={14} sizeWidth={14} />
        <p className={styles.text}>Скрыть</p>
      </li>

      <div className={styles.divider} />

      <li className={styles.menuItemDisplay} onClick={() => console.log(postId)}>
        <Icon name={EIcons.Save} sizeHeight={14} sizeWidth={14} />
        <p className={styles.text}>Сохранить</p>
      </li>

      <div className={styles.divider} />

      <li className={styles.menuItem} onClick={() => console.log(postId)}>
        <Icon name={EIcons.Warning} sizeHeight={14} sizeWidth={16} />
        <p className={styles.text}>Пожаловаться</p>
      </li>
    </ul>
  );
}
