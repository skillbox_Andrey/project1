import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import styles from './textcontent.css';
import { Post } from '../../../Post';
import { Link } from 'react-router-dom';


interface ITextContentProps {
  postLink: string
  srcAvatar: string
  userName: string
}

export function TextContent({ postLink, srcAvatar, userName }: ITextContentProps) {
  const [isModalOpened, setIsModalOpened] = useState(false)


  return (
    <div className={styles.textContent}>
      <div className={styles.metaData}>
        <div className={styles.userLink}>
          <img className={styles.avatar} src={srcAvatar} alt="" />
          <a className={styles.username} href="#user-link">{userName}</a>
        </div>
        <span className={styles.createdAt}>
          <span className={styles.publishedLabel}>опубликовано </span>
          1 час назад
        </span>
      </div>
      <h2 className={styles.title}>
        <Link className={styles.postLink} to="/posts/:id" onClick={() => setIsModalOpened(true)}>
          {postLink}
        </Link>
      </h2>
      {/* {
        isModalOpened && (
          <Post onClose={() => setIsModalOpened(false)} />
        )
      } */}
    </div>
  );
}
