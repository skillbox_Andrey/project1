import React from 'react';
import styles from './card.css';
import { TextContent } from "./TextContent";
import { Preview } from "./Preview";
import { Menu } from "./Menu";
import { Controls } from "./Controls";
import { amountCards } from '../../../hooks/usePosts';

interface ICardProps {
  postLink: string
  srcAvatar: string
  srcPreview: string
  userName: string
  key: string
}
let arrPosts = []
export function Card({ postLink, srcAvatar, srcPreview, userName, key }: ICardProps) {

  return (
    <li className={styles.card}>
      <TextContent postLink={postLink} srcAvatar={srcAvatar} userName={userName} />
      <Preview srcPreview={srcPreview} />
      <Menu MyId={key} />
      <Controls />
    </li>
  );
}
