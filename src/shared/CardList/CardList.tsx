import React, { useEffect, useState, useContext, createElement, ReactNode, useRef } from 'react';
import styles from './cardlist.css';
import { Card } from "./Card";
import axios from 'axios'
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import { Route, Routes } from 'react-router-dom';
import { Post } from '../Post';

export function CardList() {

  const token = useSelector<RootState>(state => state.tokenText)
  const [posts, setPosts] = useState<any[]>([])
  const [loading, setLoading] = useState(false)
  const [errorLoading, setErrorLoading] = useState('')
  const [nextAfter, setNextAfter] = useState('')
  const bottomOfList = useRef<HTMLDivElement>(null)
  let [counterLoads, setCounterLoads] = useState(0)
  const [loadBtn, setLoadBtn] = useState(false)

  async function load() {
    setLoading(true)
    setErrorLoading('')
    try {
      const { data: { data: { after, children } } } = await axios.get('https://oauth.reddit.com/rising/', {
        headers: { Authorization: `bearer ${token}` },
        params: {
          limit: 10,
          after: nextAfter
        }
      })
      setNextAfter(after)
      setPosts(prevChildren => prevChildren.concat(...children))
    } catch (error) {
      setErrorLoading(String(error))
    }

    setLoading(false)
  }

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting) {
        if (counterLoads < 3) {
          setCounterLoads(prevCount => prevCount += 1)
          console.log('load()', counterLoads)
          load()
        } else {
          setLoadBtn(true)
        }
      }
    }, {
      rootMargin: '100px',
    })

    if (bottomOfList.current) {
      observer.observe(bottomOfList.current)
    }

    return () => {
      if (bottomOfList.current) {
        observer.unobserve(bottomOfList.current)
      }
    }
  }, [bottomOfList.current, nextAfter, token])

  function morePosts() {
    setLoadBtn(false)
    setCounterLoads(0)
    load()
  }

  return (
    <div>
      <ul className={styles.cardsList}>
        {posts.length === 0 && !loading && !errorLoading && (
          <div style={{ textAlign: 'center' }}>Нет ни одного поста</div>
        )}

        {posts.map(post => (
          <Card
            key={post.data.id}
            userName={post.data.author_fullname}
            srcAvatar={post.data.thumbnail}
            srcPreview={post.data.url}
            postLink={post.data.title}
          />
        ))}

        <div ref={bottomOfList} />

        {loading &&
          <div style={{ textAlign: 'center' }}>Загрузка...</div>
        }

        {errorLoading && (
          <div role='alert' style={{ textAlign: 'center' }}>
            {errorLoading}
          </div>
        )}

        {loadBtn && (
          <button className={styles.button} onClick={morePosts} >Загрузить ещё</button>
        )}
      </ul>
      <Routes>
        <Route path='/:id' element={<Post />} />
      </Routes>
    </div>
  )
}
