import React, { useEffect, useRef, useState } from 'react';
import ReactDOM from "react-dom";
import styles from './dropdown.css';
import { BlockIcon, WarningIcon, RepostIcon, SaveIcon, CommentIcon } from '../icons';
import { Icon } from "../icons";
import { EIcons } from '../icons';

interface IDropdownProps {
  onOpen?: () => void
  onClose?: () => void
  postId: string
  MyId: string
}

export function Dropdown(props: IDropdownProps) {
  let button = document.querySelector(`#${props.MyId}`)
  let DropdownPosition
  if (button !== null) DropdownPosition = button.getBoundingClientRect()
  console.log(DropdownPosition)
  const node = document.querySelector('#dropdown')
  console.log(DropdownPosition)
  console.log(window.pageYOffset)
  if (!node || DropdownPosition == null) return null
  return ReactDOM.createPortal((
    <div style={{ left: DropdownPosition['x'] - 60, top: DropdownPosition['y'] + 50 + window.pageYOffset, position: 'absolute', background: '#fff' }}>
      <div className={styles.listContainer} onClick={props.onClose}>
        <div className={styles.list}>
          <ul className={styles.menuItemsList}>
            <li className={styles.menuItemDisplay} onClick={() => console.log(props.postId)}>
              <Icon name={EIcons.Comment} sizeHeight={14} sizeWidth={14} />
              <p className={styles.text}>Комментарии</p>
            </li>

            <div className={styles.divider} />

            <li className={styles.menuItemDisplay} onClick={() => console.log(props.postId)}>
              <Icon name={EIcons.Repost} sizeHeight={14} sizeWidth={12} />
              <p className={styles.text}>Поделиться</p>
            </li>

            <div className={styles.divider} />

            <li className={styles.menuItem} onClick={() => console.log(props.postId)}>
              <Icon name={EIcons.Block} sizeHeight={14} sizeWidth={14} />
              <p className={styles.text}>Скрыть</p>
            </li>

            <div className={styles.divider} />

            <li className={styles.menuItemDisplay} onClick={() => console.log(props.postId)}>
              <Icon name={EIcons.Save} sizeHeight={14} sizeWidth={14} />
              <p className={styles.text}>Сохранить</p>
            </li>

            <div className={styles.divider} />

            <li className={styles.menuItem} onClick={() => console.log(props.postId)}>
              <Icon name={EIcons.Warning} sizeHeight={14} sizeWidth={16} />
              <p className={styles.text}>Пожаловаться</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  ), node)
}