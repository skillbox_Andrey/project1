import React from 'react';
import styles from './header.css';
import { SearchBlock } from "./SearchBlock";
import { ThreadTitle } from "./ThreadTitle";
import { SortBlock } from "./SortBlock";
import { UserBlock } from "./UserBlock";

export function Header() {
  return (
    <header className={styles.header}>
      <div className={styles.headerWrapper}>
        <ThreadTitle />
        <SortBlock />
        <SearchBlock />
      </div>
      <UserBlock />

    </header>
  );
}
