import React, { useContext } from 'react';
import styles from './userblock.css';
import { EColor, Text } from '../../Text';
import { IconAnonim } from "../../icons";
import axios from 'axios'
import { useUserData } from '../../../hooks/useUserData';
import { useNameStore } from '../../zustandStore';
// import { userContext } from '../../context/userContext';


export function UserBlock() {
  // const { iconImg, name } = useContext(userContext)
  const setCommentValue = useNameStore(state => state.setName)
  const [data, loading] = useUserData()
  console.log(loading, 'loading')
  console.log(data.data.name)
  if (data.data.name) setCommentValue(data.data.name)
  return (
    <a className={styles.userBox}
      href='https://www.reddit.com/api/v1/authorize?client_id=MuG02oQPZZ8uXmG-AkhXeQ&response_type=code&state=random_string&redirect_uri=http://localhost:3000/auth&duration=permanent&scope=read submit identity'>
      <div className={styles.avatarBox}>
        {data.data.iconImg
          ? <img src={data.data.iconImg} alt="user avatar" className={styles.avatarImage} />
          : <IconAnonim />
        }
      </div>

      <div className={styles.username}>
        {loading ?
          (<Text size={20} color={data.data.name ? EColor.black : EColor.gray99}>Загрузка...</Text>)
          :
          (<Text size={20} color={data.data.name ? EColor.black : EColor.gray99}>{data.data.name || 'Аноним'}</Text>)}
      </div>
    </a >
  );
}
