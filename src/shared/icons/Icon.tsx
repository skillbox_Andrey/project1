import React from 'react';
import classNames from 'classnames'
import styles from './icon.css';
import { BlockIcon } from './BlockIcon';
import { CommentIcon } from './CommentIcon';
import { MenuIcon } from './MenuIcon';
import { RepostIcon } from './RepostIcon';
import { SaveIcon } from './SaveIcon';
import { WarningIcon } from './WarningIcon';

// const names = {
//   'Block': BlockIcon,
//   'Comment': CommentIcon,
//   'Menu': MenuIcon,
//   'Repost': RepostIcon,
//   'Save': SaveIcon,
//   'Warning': WarningIcon,
// }
export enum EIcons {
  Block = 'https://raw.githubusercontent.com/7Liberal7/svgIcons/2c5b9a3b12f16d658c18a7938170351a32f73cca/BlockIcon.svg',
  Comment = 'https://raw.githubusercontent.com/7Liberal7/svgIcons/2c5b9a3b12f16d658c18a7938170351a32f73cca/CommentIcon.svg',
  Repost = 'https://raw.githubusercontent.com/7Liberal7/svgIcons/2c5b9a3b12f16d658c18a7938170351a32f73cca/RepostIcon.svg',
  Save = 'https://raw.githubusercontent.com/7Liberal7/svgIcons/2c5b9a3b12f16d658c18a7938170351a32f73cca/SaveIcon.svg',
  Warning = 'https://raw.githubusercontent.com/7Liberal7/svgIcons/2c5b9a3b12f16d658c18a7938170351a32f73cca/WarningIcon.svg',
}

interface ITextProps {
  sizeHeight: number
  sizeWidth: number
  name: string
}

export function Icon(props: ITextProps) {
  const {
    sizeHeight,
    sizeWidth,
    name = EIcons
  } = props

  return (
    <img
      //  src="https://leonardo.osnova.io/b6bf9675-945c-e142-5486-75ad3831408d/-/preview/700/-/format/webp/"
      src={`${name}`}

      alt="" width={sizeWidth} height={sizeHeight} />
    // <div className={classes} >
    //   <img src="1.svg" alt="" width={size} width={size} />
    // </div>
  );
}
