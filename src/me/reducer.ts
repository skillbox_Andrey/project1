import { Reducer } from "react";
import { IUserData, MeRequestAction, meRequestErrorAction, meRequestSuccessAction, ME_REQUEST, ME_REQUEST_ERROR, ME_REQUEST_SUCCESS } from "./action";

export type MeState = {
  loading: boolean
  error: string
  data: IUserData
}

type MeActions = MeRequestAction | meRequestSuccessAction | meRequestErrorAction

export const meReducer: Reducer<MeState, MeActions> = (state, action) => {
  switch (action.type) {
    case ME_REQUEST:
      return {
        ...state,
        loding: true
      }
    case ME_REQUEST_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false
      }
    case ME_REQUEST_SUCCESS:
      return {
        ...state,
        data: action.data
      }
    default:
      return state
  }
}