import { useDispatch, useSelector, useStore } from 'react-redux';
import { ActionCreator, Action, AnyAction } from "redux"
import { ThunkAction } from 'redux-thunk';
import { RootState } from "../store"
import axios from "axios";

// interface Action {
//   T: any
// }

// function Action<Action>(arg: any): Action {
//   return arg;
// }

export const ME_REQUEST = 'ME_REQUEST'
export type MeRequestAction = {
  type: typeof ME_REQUEST
}

export const meRequest:
  ActionCreator<MeRequestAction> = () => ({
    type: ME_REQUEST
  })

export interface IUserData {
  name?: string
  iconImg?: string
}

export const ME_REQUEST_SUCCESS = 'ME_REQUEST_SUCCESS'
export type meRequestSuccessAction = {
  type: typeof ME_REQUEST_SUCCESS
  data: IUserData
}

export const meRequestSuccess: ActionCreator<meRequestSuccessAction> = (data: IUserData) => ({
  type: ME_REQUEST_SUCCESS,
  data,
})

export const ME_REQUEST_ERROR = 'ME_REQUEST_ERROR'
export type meRequestErrorAction = {
  type: typeof ME_REQUEST_ERROR
  error: string
}


export const meRequestError: ActionCreator<meRequestErrorAction> = (error: string) => ({
  type: ME_REQUEST_ERROR,
  error: error
})
export const meRequestAsync =
  (): ThunkAction<void, RootState, unknown, AnyAction> =>
    (dispatch, getState) => {
      dispatch(meRequest())
      if (getState().tokenText !== undefined) {
        axios.get('https://oauth.reddit.com/api/v1/me',
          {
            headers: {
              Authorization: `bearer ${getState().tokenText}`
            }
          }).then((resp) => {
            const userData = resp.data
            dispatch(meRequestSuccess({ name: userData.name, iconImg: userData.icon_img }))
          }).catch((error) => {
            console.log(String(error))
            dispatch(meRequestError())
          })
      }
    } 