import { ActionCreator, AnyAction } from 'redux';
import { useDispatch, useSelector, useStore } from 'react-redux';
import { ME_REQUEST, ME_REQUEST_ERROR, ME_REQUEST_SUCCESS } from './me/action';
import { meReducer, MeState } from './me/reducer';
export type RootState = {
  commentText: string,
  tokenText: string
  me: MeState
}

const initialState: RootState = {
  commentText: 'не начал писать коммент',
  tokenText: '',
  me: {
    loading: false,
    error: 'все норм',
    data: {

    }
  }
}

const UPDATE_COMMENT = 'UPDATE_COMMENT'

const UPDATE_TOKEN = 'UPDATE_TOKEN'

export const updateComment: ActionCreator<AnyAction> = (text) => ({
  type: UPDATE_COMMENT,
  text,
})

export const updateToken: ActionCreator<AnyAction> = (text) => ({
  type: UPDATE_TOKEN,
  text,
})

export const PostsState: ActionCreator<AnyAction> = (bool) => ({
  type: 'goToPosts',
  bool,
})


export const rootReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case UPDATE_COMMENT:
      return {
        ...state,
        commentText: action.text
      }

    case UPDATE_TOKEN:
      return {
        ...state,
        tokenText: action.text
      }
    case ME_REQUEST:
    case ME_REQUEST_SUCCESS:
    case ME_REQUEST_ERROR:
      return {
        ...state,
        me: meReducer(state.me, action),
      }
    default:
      break;
  }
  return state
}