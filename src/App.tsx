// import React, { useEffect, useState, useContext, createElement, Reducer } from 'react'
import './main.global.css'
import { hot } from 'react-hot-loader/root'
import { Layout } from "./shared/Layout";
import { Header } from "./shared/Header";
import { Content } from "./shared/Content";
import { CardList } from "./shared/CardList"
// import { useToken } from './hooks/useToken';
import { usePosts } from './hooks/usePosts';
import { usePostsData } from './hooks/usePostsData';
// import { tokenContext } from './shared/context/tokenContext';
// import { UserContextProvider } from './shared/context/userContext';
import { rootReducer, RootState } from './store';
// import { useDispatch, useSelector, useStore } from 'react-redux';
import { ActionCreator, AnyAction, applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from "redux-devtools-extension";
import { useEffect, useState } from 'react';
import { saveToken } from './hooks/dispatchToken';
import thunk from 'redux-thunk';
import { BrowserRouter, Navigate, Route, Routes, useNavigate } from 'react-router-dom'
// import { PostsState } from './store';
import { Post } from './shared/Post';
import { NotFound } from './shared/NotFound';
import { RecoilRoot } from 'recoil';
// import { useStore } from "./store";

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))


function AppComponent() {
  const [mounted, setMounted] = useState(false)
  useEffect(() => {
    setMounted(true)
  }, [])

  useEffect(() => {
    store.dispatch(saveToken())
  }, [])

  return (
    <Provider store={store}>
      {mounted &&
        <BrowserRouter>
          <RecoilRoot>
            <Layout>
              <Header />
              <Content>
                <Routes>
                  <Route path='/posts' element={<CardList />} />
                  <Route path='/posts/*' element={<CardList />} />
                  <Route path='auth' element={<Navigate to={'/posts'} />} />
                  <Route path='/' element={<Navigate to={'/posts'} />} />
                  <Route path='*' element={<NotFound />} />
                </Routes>
              </Content>
            </Layout>
          </RecoilRoot>
        </BrowserRouter>
      }
    </Provider>
  )
}

export const App = hot(() => <AppComponent />)