export const indexTemplate = (content, token, listPosts) =>
  `
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="/static/client.js" type="application/javascript"></script>
  <script>
    window.__token__ = '${token}'
    window.__listPosts__ = ${listPosts}
  </script>
  <title>Reddit</title>
</head>

<body style="position:relative">
  <div id="react_root">${content}</div>
  <div id="react_modal" style="
    z-index: 1000;
    position: absolute;
    left: 0;
    top: 190px;
    width: 100%;
  ">
    <div id="react_CommentForm" style="
      z-index: 1000;
      position: absolute;
      left: 0;
      top: 0;
    ">
      <div id="bool_Comment" style="display: none">false</div>
    </div>
  </div>
   <div id="dropdown"</div>
</body>

</html>
`;