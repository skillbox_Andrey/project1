import express from "express";
import ReactDOM from "react-dom/server";
import { App } from "../App";
import { indexTemplate } from "./indexTemplate";
import axios from "axios";
import { redirect } from "react-router";

const app = express();
app.use("/static", express.static("./dist/client"));

export var Token = ''

app.get("/auth", (req, res) => {
  axios.post(
    'https://www.reddit.com/api/v1/access_token',
    `grant_type=authorization_code&code=${req.query.code}&redirect_uri=http://localhost:3000/auth`,
    {
      auth: { username: process.env.CLIENT_ID, password: 'dbOHZBD4jil2b4N4f2U6cw5JLYFDxg' },
      headers: { 'Content-type': 'application/x-www-form-urlencoded' }
    }
  ).then(({ data }) => {
    Token = data['access_token']

    axios.get(
      'https://oauth.reddit.com/best.json?sr_detail=true',
      {
        headers: {
          'Authorization': `bearer ${Token}`
        }
      }
    ).then(({ data }) => {
      res.send(indexTemplate(ReactDOM.renderToString(App()), Token, data[data],))
    })
  }).catch(console.log)
});


app.get("*", (req, res) => {
  res.send(indexTemplate(ReactDOM.renderToString(App())));
});

app.listen(3000, () => {
  console.log("server started on port http://localhost:3000");
});