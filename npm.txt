npm install --global webpack@4.42.0 webpack-cli@3.3.11
npm install typescript@4.6.4 -g
npm install react@17.0.1 react-dom@17.0.1
npm i -D webpack@4.42.0 webpack-cli@3.3.11
npm i -D typescript@4.6.4 ts-loader@6.2.1
npm i -D html-webpack-plugin@4
npm i -D webpack-dev-server@3.10.3
npm i -D cross-env
npm i express
npm i -D webpack-node-externals
npm i nodeman
npm i -D webpack-dev-middleware webpack-hot-middleware react-hot-loader
npm i -D clean-webpack-plugin
npm i -D @hot-loader/react-dom --legacy-peer-deps
npm i -D style-loader@1.1.3 css-loader@3.4.2
npm i -D less-loader@5.0.0 less 
npm i -D @types/react @types/react-dom
npm install -g yo
npm i generator-react-ts-component-dir
npm i jest
npm i -D enzyme
npm i -D enzyme enzyme-adapter-react @types/enzyme    
npm i -D identity-obj-proxy
npm i --save-dev @types/enzyme-adapter-react-16
npm i classnames
npm i -D @types/classnames  
npm i axios
npm i react-redux
npm i redux
npm i --save redux-devtools-extension
npm i --save redux-thunk
npm i --save formik
npm i react-router-dom
npm i @types/react-router-dom
npm i zustand